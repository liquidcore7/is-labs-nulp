#include <string>
#include <vector> 
#include "labs.h"
#include <fstream>

using namespace std::string_literals;



int main() {

    std::locale::global(std::locale(""));
    std::wifstream source("../lab5_source.txt");
    std::wofstream sink  ("../lab5_sink.txt");

    std::wstring plain_text;

    while ( source >> plain_text ) {

        std::vector<wchar_coproduct> plain_text_mapped;
        for (const wchar_t& wc : plain_text) {
            plain_text_mapped.emplace_back(wc);
        }

        const auto bigrams = bigram<wchar_coproduct>::split_vec(plain_text_mapped);

        for (const auto& bigram : bigrams) {
            sink << char_mapping.encode_bigram(bigram);
        }

        sink << std::endl;
    }
    
    source.close();
    sink.close();

    return 0;
}