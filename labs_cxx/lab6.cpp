#include <string>
#include <vector>
#include "labs.h"
#include <fstream>

using namespace std::string_literals;



int main() {

    std::locale::global(std::locale(""));
    std::wifstream source("../lab5_sink.txt");

    std::wstring encoded_text;

    while ( source >> encoded_text ) {

        std::vector<wchar_coproduct> encoded_text_mapped;
        for (const wchar_t& wc : encoded_text) {
            encoded_text_mapped.emplace_back(wc);
        }

        const auto bigrams = bigram<wchar_coproduct>::split_vec(encoded_text_mapped);

        for (const auto& bigram : bigrams) {
            std::wcout << char_mapping.decode_bigram(bigram);
        }

        std::wcout << std::endl;
    }

    source.close();

    return 0;
}