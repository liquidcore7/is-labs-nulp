#include <vector>
#include <iostream>
#include <unordered_set>
#include <random>
#include <iterator>


template <typename T>
using matrix_t = std::vector<std::vector<T>>;


static std::random_device rd;
static std::mt19937 rand_generator(rd());


template <typename T>
class bigram {
    T head;
    T tail;

    public:
    bigram(const T* from_array) : head(from_array[0]), tail(from_array[1]) {}
    bigram(const T h, const T t) : head(h), tail(t) {}
    bigram(const bigram<T>&) = default;
    bigram(bigram<T>&&) = default;


    const T getHead() const {
        return head;
    }

    const T getTail() const {
        return tail;
    }

    const static std::vector<bigram<T>> split_vec(const std::vector<T>& arg) {
        std::vector<bigram> container;

        const auto pre_end = arg.end() - 1;

        for (auto it = arg.begin(); it < pre_end; ++it) {
            const auto h = *it++;
            const auto t = *it;
            container.emplace_back(h, t);
        }

        return container;
    }
};

template <typename T>
bool operator==(const bigram<T>& l, const bigram<T>& r) {
    return (l.getHead() == r.getHead()) && (l.getTail() == r.getTail());
}

template <typename T>
std::wostream& operator<<(std::wostream& wos, const bigram<T> bg) {
    wos << bg.getHead() << bg.getTail();
    return wos;
}




template <size_t N, size_t M, typename T>
class matrix {

    std::vector<std::vector<T>> data;

    const bigram<size_t> linear_find(const T& searched) const {
        for (size_t n = 0; n < N; ++n) {
            for (size_t m = 0; m < M; ++m) {
                if (data[m][n] == searched) {
                    return bigram<size_t>(m, n);
                }
            }
        }
        return bigram<size_t>(-1, -1);
    }

    const size_t advance_right(const size_t& from_pos) const {
        return (from_pos + 1) % M;
    }

    const size_t advance_down(const size_t& from_pos) const {
        return (from_pos + 1) % N;
    }

    const size_t advance_left(const size_t& from_pos) const {
        return from_pos > 0 ? (from_pos - 1) : (N - 1);
    }

    const size_t advance_up(const size_t& from_pos) const {
        return from_pos > 0 ? (from_pos - 1) : (M - 1);
    }

    const T elem_at(const bigram<size_t>& pos) const {
        return data[pos.getHead()][pos.getTail()];
    }

  public:

    matrix(const matrix_t<T>& from_array): data(from_array) {}

    matrix(std::istream&& is) {
        for (size_t n = 0; n < N; ++n) {
            for (size_t m = 0; m < M; ++m) {
                is >> data[m][n];
            }
        }
    }


    const bigram<T> encode_bigram(const bigram<T>& inp) const {
        const auto cornerH = linear_find(inp.getHead());
        const auto cornerT = linear_find(inp.getTail());


        if (cornerH.getHead() == cornerT.getHead()) { // same row
            const auto row = cornerH.getHead();
            return bigram<T>(
                data[row][ advance_right(cornerH.getTail()) ],
                data[row][ advance_right(cornerT.getTail()) ]
            );
        } else if (cornerH.getTail() == cornerT.getTail()) { // same column
            const auto column = cornerH.getTail();
            return bigram<T>(
                data[ advance_down(cornerH.getHead()) ][column],
                data[ advance_down(cornerT.getHead()) ][column]
            );
        } else if (cornerH == cornerT) { // same pos
            const T elem = elem_at(bigram<size_t>(cornerH.getHead(), advance_right(cornerH.getTail())));
            return bigram<T>(elem, elem);
        } else { // form a rectangle
            const auto swappedH = bigram<size_t>(cornerH.getHead(), cornerT.getTail());
            const auto swappedT = bigram<size_t>(cornerT.getHead(), cornerH.getTail());

            return bigram<T>(
                elem_at(swappedH),
                elem_at(swappedT)
            );
        }

    }

    const bigram<T> decode_bigram(const bigram<T>& inp) const {
        const auto cornerH = linear_find(inp.getHead());
        const auto cornerT = linear_find(inp.getTail());


        if (cornerH.getHead() == cornerT.getHead()) { // same row
            const auto row = cornerH.getHead();
            return bigram<T>(
                    data[row][ advance_left(cornerH.getTail()) ],
                    data[row][ advance_left(cornerT.getTail()) ]
            );
        } else if (cornerH.getTail() == cornerT.getTail()) { // same column
            const auto column = cornerH.getTail();
            return bigram<T>(
                    data[ advance_up(cornerH.getHead()) ][column],
                    data[ advance_up(cornerT.getHead()) ][column]
            );
        } else if (cornerH == cornerT) { // same pos
            const T elem = elem_at(bigram<size_t>(cornerH.getHead(), advance_left(cornerH.getTail())));
            return bigram<T>(elem, elem);
        } else { // form a rectangle
            const auto swappedH = bigram<size_t>(cornerH.getHead(), cornerT.getTail());
            const auto swappedT = bigram<size_t>(cornerT.getHead(), cornerH.getTail());

            return bigram<T>(
                    elem_at(swappedH),
                    elem_at(swappedT)
            );
        }

    }
};


class wchar_coproduct {
    wchar_t main;
    std::optional<wchar_t> additional;


  public:
    wchar_coproduct(const wchar_t& from_single_char) : main(from_single_char), additional(std::nullopt) {}
    wchar_coproduct(const std::initializer_list<wchar_t>& coll) : main(*coll.begin()) {
        const auto next = coll.begin() + 1;
        if (next == coll.end()) {
            this->additional = std::nullopt;
        } else {
            this->additional = std::make_optional(*next);
        }
    }
    wchar_coproduct(const wchar_coproduct&) = default;
    wchar_coproduct(wchar_coproduct&&) = default;


    bool does_intersect_with(const wchar_coproduct& other) const {
        return (
                (main == other.main) ||
                (main == other.additional.value_or(L'\0')) ||
                (additional.value_or(L'\0') == other.main) ||
                (additional.value_or(L'\0') == other.additional.value_or(L'\b'))
               );
    }
    
    const wchar_t pick_any() const {
        if (additional.has_value()) {
            if ((rd() % 2) == 0) return additional.value();
            else return main;
        } else return main;
    }

};

bool operator==(const wchar_coproduct& l, const wchar_coproduct& r) {
    return l.does_intersect_with(r);
}

std::wostream& operator<<(std::wostream& wos, const wchar_coproduct& wc) {
    wos << wc.pick_any();
    return wos;
}




const static matrix<5, 5, wchar_coproduct> char_mapping({
    {L'а', L'б', L'в', {L'г', L'ш'}, {L'ґ', L'х'}},
    {L'д', L'е', {L'є', L'ц'}, {L'ж', L'ч'}, {L'з', L'щ'}},
    {L'и', L'і', {L'ї', L'ю'}, {L'й', L'ь'}, L'к'},
    {L'л', L'м', L'н', L'о', L'п'},
    {L'р', L'с', L'т', L'у', {L'ф', L'я'}}
});

