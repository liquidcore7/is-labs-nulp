from typing import *
from dataclasses import dataclass
from math import copysign


@dataclass
class ShiftCipherSettings:
    language_first_char: int
    language_num_chars: int


class ShiftCipher:

    UKR = ShiftCipherSettings(ord('А'), 33 * 2)
    EN  = ShiftCipherSettings(ord('A'), 26 * 2)

    def __shift_char_impl__(self, inp: str, offset: int) -> str:
        return chr(
            ((ord(inp) - self.settings.language_first_char + offset) % self.settings.language_num_chars) + self.settings.language_first_char
        )


    def __shift_cipher_impl__(self, inp: str, offset: int) -> str:
        normalized_offset = int(
            copysign(abs(offset) % self.settings.language_num_chars, offset)
        )

        return ''.join(list(map(lambda x: self.__shift_char_impl__(x, normalized_offset), inp)))

    def __init__(self, offset: int, settings: ShiftCipherSettings):
        self.offset = offset
        self.settings = settings

    def encode(self, inp: str) -> str:
        return self.__shift_cipher_impl__(inp, self.offset)

    def decode(self, inp: str) -> str:
        return self.__shift_cipher_impl__(inp, -self.offset)




if __name__ == "__main__":

    cipher = ShiftCipher(22, ShiftCipher.UKR)
    original = []
    with open('source.txt', 'r') as encoded_file:
        for line in encoded_file:
            original.append(line)

    print('Original text:\n' + '\n'.join(original), end = '\n\n')

    encoded = [cipher.encode(x) for x in original]
    print('Encoded text:\n' + '\n'.join(encoded), end = '\n\n')

    decoded = [cipher.decode(x) for x in encoded]
    print('Decoded text:\n' + '\n'.join(decoded), end = '\n\n')