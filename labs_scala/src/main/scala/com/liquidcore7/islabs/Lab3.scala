package com.liquidcore7.islabs

import java.io.File

import scala.io.Source


case class Decoder(private val ofMapping: Map[Int, Char]) {

  def decode(text: String): String =
    text
      .sliding(2, 2)
      .map(_.toInt)
      .map(ofMapping)
      .mkString("")

}

object StatsUtils {
  def relativeFrequency[T](of: TraversableOnce[T], element: T): Double = 100.0 * of.count(_ == element) / of.size
}


object Lab3 extends App {

  val table: Map[Char, Seq[Int]] =
    Source
      .fromFile(new File("table.txt"))
      .getLines
      .map { line =>
        val parsed = line.split(",")
        parsed.head.head -> parsed.tail.map(_.toInt).toSeq
      }.toMap

  val reversedTable: Map[Int, Char] = table.flatMap {
    case (key, values) => values.map(_ -> key)
  }

  val decoder = Decoder(reversedTable)
  val encodedData = Source.fromFile(new File("output_2.txt")).mkString
  val decodedText = decoder decode encodedData

  val previousText = Source.fromResource("source_2.txt").mkString.toLowerCase
  println(s"The texts ${if (previousText == decodedText) "are" else "aren't"} equal.\nSource: $previousText\nDecoded: $decodedText")


  val relativeLetterFreq = previousText.map {
    elem => elem -> StatsUtils.relativeFrequency(previousText, elem)
  }.toMap

  println(relativeLetterFreq map { case (k, v) => s"$k  |  $v"} mkString "\n")

  val cryptogramFreq = {
    val domain = encodedData.sliding(2, 2).map(_.toInt).toSeq

    domain.distinct.map {
      elem => elem -> StatsUtils.relativeFrequency(domain, elem)
    }.toMap
  }
  println(cryptogramFreq map { case (k, v) => s"$k  |  $v"} mkString "\n")

}
