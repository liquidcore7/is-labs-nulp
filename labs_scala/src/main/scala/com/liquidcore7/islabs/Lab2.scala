package com.liquidcore7.islabs

import java.io.PrintWriter

import scala.io.Source
import scala.util.{Failure, Random, Success, Try}


trait CharMapping {

  import CharMapping.random

  protected type OutRepr = Int
  protected val mapping: Map[Char, Seq[OutRepr]]
  protected def outReprShow: OutRepr => String = {
    case lessThanTen if lessThanTen < 10 => "0" + lessThanTen.toString.take(1)
    case larger => larger.toString
  }

  def encodeSymbol(input: Char): String = {
    val possibleEncodings = mapping(input)
    val randomIndex = random nextInt possibleEncodings.length

    possibleEncodings andThen outReprShow apply randomIndex
  }

  def dump: String = mapping.map {
    case (key, values) => (key.toString +: values.map(outReprShow)).mkString(",")
  }.mkString("\n")
}

object CharMapping {
  implicit val random: Random = new Random()

  def apply(fromPercentageMap: Map[Char, Int]): Try[CharMapping] = {
    val totalPercentage = fromPercentageMap.values.sum
    for {
      range <- if (totalPercentage < 100) Success(0 to totalPercentage)
               else Failure(new RuntimeException(s"Total symbol percentage is above 100: $totalPercentage"))
      shuffledRange = random shuffle range.toList
      symbolMapping = fromPercentageMap.foldLeft(0 -> Map.empty[Char, Seq[Int]]) {
        case ((lastPosRead, mapping), (symbol, percent)) =>
          (lastPosRead + percent) -> (mapping + (symbol -> shuffledRange.slice(lastPosRead, lastPosRead + percent)))
      }._2
      charMapping = new CharMapping {
        override protected val mapping: Map[Char, Seq[OutRepr]] = symbolMapping
      }
    } yield charMapping
  }
}


object IOUtils {
  def getPercentageMap: Map[Char, Int] =
    Source
      .fromResource("freqtable.txt")
      .getLines
      .toSeq
      .map(line => {
        val parsed = line.split(":")
        parsed(0)(0) -> parsed(1).toInt
      }).toMap
}




object Main extends App {

  val percentageMap = IOUtils.getPercentageMap


  val textToEncode =
    Source
      .fromResource("source_2.txt")
      .getLines
      .map(_.toLowerCase)
      .mkString("")

  val outFile = new PrintWriter("output_2.txt")
  val tableOutFile = new PrintWriter("table.txt")

  val result = for {
    ukrainianCharMap <- CharMapping(percentageMap)

    _ <- Try(tableOutFile write ukrainianCharMap.dump)
    _ <- Try(tableOutFile.close)

    encodedText = textToEncode.flatMap(ukrainianCharMap.encodeSymbol)

    _ <- Try(outFile write encodedText)
    _ <- Try(outFile.close)
  } yield encodedText

  result.get

}