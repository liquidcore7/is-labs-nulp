from lab1 import ShiftCipherSettings, ShiftCipher


class GammaCipher(ShiftCipher):

    def __get_offset__(self, of: str) -> int:
        return ord(of) - self.settings.language_first_char 

    def __normalize_offset__(self, offset: int) -> int:
        return int(
            copysign(abs(offset) % self.settings.language_num_chars, offset)
        )

    def __gamma_cipher_impl__(self, inp: str, gamma: str, pos_multiplier: int) -> str:
        replied_gamma = gamma * (len(inp) // len(gamma) + 1)

        return ''.join(list(map(lambda tupl: self.__shift_char_impl__(tupl[0], pos_multiplier * self.__get_offset__(tupl[1])), 
                    zip(inp, replied_gamma))))



    def __init__(self, gamma: str, settings: ShiftCipherSettings):
        self.gamma = gamma
        self.settings = settings

    def encode(self, inp: str) -> str:
        return self.__gamma_cipher_impl__(inp, self.gamma, 1)

    def decode(self, inp: str) -> str:
        return self.__gamma_cipher_impl__(inp, self.gamma, -1)


if __name__ == "__main__":

    cipher = GammaCipher('менторство', ShiftCipher.UKR)
    original = ['КафедраПрикладноїМатематики']
    # with open('source.txt', 'r') as encoded_file:
    #     for line in encoded_file:
    #         original.append(line)

    print('Original text:\n' + '\n'.join(original), end = '\n\n')

    encoded = [cipher.encode(x) for x in original]
    print('Encoded text:\n' + '\n'.join(encoded), end = '\n\n')

    decoded = [cipher.decode(x) for x in encoded]
    print('Decoded text:\n' + '\n'.join(decoded), end = '\n\n')